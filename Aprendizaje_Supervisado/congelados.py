from os import pipe

import numpy as np
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
from unidecode import unidecode
from string import digits
import chardet

# Lectura del dataset Productos y hacemos copia. 
path="C:\\Users\\mcattaneo\\Desktop\\DS\\1°\\Entregable2\\"

def carga_dataset(archivo):
    df_carga = pd.read_csv(path + archivo + '.csv', delimiter=';', encoding='latin-1')
    return df_carga

def union_datasets(productos_df, categorias_df, ventas_df, localidades_df, provincias_df, paises_df,pdv_df):
    
    productos_categoria=pd.merge(productos_df,categorias_df,on='id_categoria').rename(columns={'nombre':'categoria'})

    #products removal
    productos_totales = ['Grupo genérico inserción','vacia','VACIO','nuevo','09 - DISPENSER',
    '10 - CARTELERIA, INSTITUCIONALES Y PLOTEOS','07 - ACCESORIOS, UTENSILIOS Y REPUESTOS ',
    '08 - INDUMENTARIA ','06 - PRODUCTOS DE LIMPIEZA ','11 - EQUIPOS','12 - MUEBLES',
    '20 - SISTEMAS DE FRÍO A PEDIDO ','0001 - GRIDO MARKET','0005 - CARTELERIA, INSTITUCIONALES Y PLOTEOS',
    '0001 - INDUMENTARIA ','0006 - EQUIPOS','0008 - MUEBLES','12 - REPUESTOS EQUIPOS DE FRIO',
    '0007 - ACCESORIOS Y REPUESTOS PARA EQUIPOS DE FRIO','0012 - MATERIAL MKT','000 - ARTICULOS PROTECCION COVID-19','0006 - PRODUCTOS COMPOSTABLES /BIODEGRADABLES',
    '00556 - PRODUCTOS BIODEGRADABLES / COMPOSTABLES','0003 - ARTICULOS PROTECCION COVID-19',
    '0000 - PRODUCTOS DE LIMPIEZA ','0002 - ACCESORIOS, UTENSILIOS Y REPUESTOS ',
    '0004 - DISPENSER','0002 - PRODUCTOS ESCOLARES','0003 - LIQUIDO PRODUCTO SP','0003 - ALMACEN - COMESTIBLES',
    '0003 - ALMACEN', '0005 - ENVASES TERMICOS Y VASOS','0009 - DESCARTABLES ','0010 - PACKAGING']
    productos_categoria2 = productos_categoria[~productos_categoria['categoria'].isin(productos_totales)]
    
    ventas_producto = pd.merge(ventas_df,productos_categoria2,on='sku')
    ventas_producto.presentacion.replace(" ","Baldes", inplace= True)
   
    pdv_ubicacion=pdv_df.merge(localidades_df,on='id_Localidad').rename(columns={'nombre_x':'Punto_Venta','nombre_y':'Localidad'})
    pdv_ubicacion=pdv_ubicacion.merge(provincias_df,on='id_Provincia').rename(columns={'nombre':'Provincia'})
    pdv_ubicacion=pdv_ubicacion.merge(paises_df,on='id_Pais').rename(columns={'nombre':'Pais'})
 
    ventas_producto_pdv = ventas_producto.merge(pdv_ubicacion,on='id_punto_venta',how="inner")

    ventas_producto_pdv.drop(columns=['id_Pais', 'Pais'], inplace=True)
    ventas_producto_pdv['aniomes'] = ventas_producto_pdv['fecha'].apply(lambda x: x.strftime('%Y%m'))
    return  ventas_producto_pdv
    
#remove accents
def remove_accents(df, columns):
    columns = list(columns)
    if len(columns) !=0:
        for i in range(len(columns)):
            if columns[i] in df.columns:
                df[columns[i]]=df[columns[i]].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    else:
        print("Seleccione una columna o la columna no se encuentra en el DataFrame")
    return df    

    #Extracción de cantidades pedidas de valor negativo y cero y outliers mediante IQR
def outliers_iqr(df,column):
    Q_1 = df[column].quantile(0.25)
    Q_3 = df[column].quantile(0.75)
    IQR_b = Q_3 - Q_1
    BI_Calculadob = (Q_1 - 1.5* IQR_b)
    BS_Calculadob = (Q_3 + 1.5* IQR_b)
    column_sin_outliersb = (df[column] >= BI_Calculadob) & (df[column] <= BS_Calculadob)
    return df[column_sin_outliersb]

    # Standardization of column name formatting (lower case)
def low_case_columns(df):
    df.rename(columns={'Punto_Venta':'punto_venta','id_Localidad':'id_localidad',
    'Localidad':'localidad','id_Provincia':'id_provincia','Provincia':'provincia','id_Pais':'id_pais','Pais':'pais'},inplace=True)
    return df

    # Removal of accents, numbers and punctuation marks from records. All entries are converted to lowercase.
def depuracion(df):
    df["presentacion_depurada"]=df.presentacion.str.lower()
    df["categoria_depurada"]=df.categoria.str.lower()
    sincero=df.categoria_depurada.str.split(" - ")
    lst2 = [item[1] for item in sincero]
    df["categoria_depurada"]=lst2
    df["marca_depurada"]= df.marca.str.lower()
    return df

    #New feature "locprov"
def locprov_col (df):
    df['locprov']=df['provincia'] + df['localidad'].str.pad(width=2,fillchar='0')
    return df

#selection of most frequent values regarding locprov and sku and rename of less frequent categories
def most_frequent(df):
    locprov_count = df.locprov.value_counts(ascending=False).reset_index().rename(columns={'index': "locprov","locprov": 'frecuencia_locprov'})
    locprov_count['frec_relativa']= (locprov_count['frecuencia_locprov']/len(df.locprov))*100
    locprov_frec=locprov_count[locprov_count['frec_relativa']>0.20]
    df.loc[~df['locprov'].isin(locprov_frec['locprov']),'locprov'] = 'otras_localidades'
    sku_count = df.sku.value_counts(ascending=False).reset_index().rename(columns={'index': 'sku', 'sku': 'frecuencia'})
    sku_count['frec_relativa']= (sku_count['frecuencia']/(sku_count['frecuencia'].sum()))*100
    sku_frec=sku_count[sku_count['frec_relativa']>1.25]
    df.loc[~df['sku'].isin(sku_frec['sku']),'sku'] = 'otros_sku'
    return df

# groupby on 'aniomes','locprov','sku'
def grouped_df(df):
    df_new=df.groupby(['aniomes','locprov','sku'],as_index=False).agg({"cantidad_pedida":sum,"unidadkg":sum}).sort_values('aniomes', ascending=True)
    df_new.reset_index()
    return df_new

# Rolling_function
def rolling_df(pdf, pwindow, column , group_col):
    df=pdf.copy()
    df['MA_{}'.format(pwindow)]  = df.groupby(group_col,as_index=False)[column].transform(lambda x: x.rolling(pwindow).mean())
    return df

# Shift function
def shift_df(df, tminus, shifted_column, groupCol, dropna):
    for i in range(1, tminus+1):
        df['t-{}'.format(i)] = df.groupby(groupCol,as_index=False)[shifted_column].shift(i)
    if dropna:
        df = df.dropna()
        df = df.reset_index(drop=True)
    return df

#Creating new feature totalkg
def total_kg (df):
    df['totalkg']=(df.cantidad_pedida)*(df.unidadkg)
    newdf= outliers_iqr(df,"totalkg")
    return newdf

# Dummies on aniomes, loprov, and sku
def getdummies (df):
    new_df_dummies=pd.get_dummies(df,columns= ["aniomes","locprov","sku"])
    return new_df_dummies

def drop_cols(df):
    dropped_df= df.drop(columns=["unidadkg"])
    return dropped_df

def save_file_csv(df):
    df.to_csv("congelados.csv")

# Definición del main
def main():

    # Importación de datos 
    productos= carga_dataset('productos')
    categorias= carga_dataset('categorias')
    pdv = carga_dataset('puntos_de_venta')
    ventas = carga_dataset('ventas')
    paises = carga_dataset('paises')
    provincias = carga_dataset('provincias')
    localidades = carga_dataset('localidades')

    # Copia de dataset y transformaciones de datos
    productos_copy = productos.copy()
    productos_copy["unidadkg"]=pd.to_numeric(productos_copy["unidadkg"])
    categorias_copy = categorias.copy()
    pdv_copy = pdv.copy()
    ventas['fecha']= ventas['anio'].astype(str) + '-' + ventas['mes'].astype(str).str.zfill(2) + '-' + ventas['dia'].astype(str).str.zfill(2)
    ventas['fecha']= pd.to_datetime(ventas['fecha'], format='%Y-%m-%d')
    ventas_copy= ventas.copy()
    ventas_copy= ventas_copy.drop(ventas_copy[ventas_copy['cantidad_pedida']<0].index)
    paises_copy= paises.copy()
    provincias_copy= provincias.copy()
    localidades_copy= localidades.copy()

    # Merge de datasets
    ventas_producto_pdv= union_datasets(productos,categorias,ventas,localidades,provincias,paises,pdv)
    
    #Remove accents
    ventas_producto_pdv= remove_accents(ventas_producto_pdv, columns=[ 'descripcion', 'marca', 'presentacion', 'categoria', 'Punto_Venta', 'Localidad','Provincia', 'Pais'])

    #convert columns to low_case
    ventas_producto_pdv= low_case_columns(ventas_producto_pdv)
    
    #depuracion
    ventas_producto_depurada= depuracion(ventas_producto_pdv)

    #locprov creation
    ventas_producto_depurada_locprov= locprov_col(ventas_producto_depurada)

    #Outliers cantidad_pedida
    ventas_producto_pdv_depurada_locprov= outliers_iqr(ventas_producto_depurada_locprov,"cantidad_pedida")

    #renaming categories on sku and l ocprov based on frequency
    new_df= most_frequent(ventas_producto_depurada_locprov)

    #grouped dataframe
    new_grouped_df=grouped_df(new_df)

    # Calculate moving average
    moving_average_df = rolling_df(new_grouped_df, 2, "cantidad_pedida",["locprov","sku"])

    # Calculate t-1
    df_timeseries = shift_df(moving_average_df, 1, "cantidad_pedida",["locprov","sku"],dropna=True)

    #adding new feature totalkg without outliers
    new_df_timeseries= total_kg (df_timeseries)

    #adding dummies
    df_dummies= getdummies(new_df_timeseries)

    #drop columns for final df
    df_final= drop_cols(df_dummies)

    print(df_final.shape)

    #save file
    save_file_csv(df_final)

main()